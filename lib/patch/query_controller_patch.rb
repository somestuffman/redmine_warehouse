module QueryControllerPatch

	def self.included(base)
		base.send(:include, ClassMethods)

		base.class_eval do 
			unloadable
		end
	end

	module ClassMethods
		def redirect_to_product_query(options)
    		redirect_to products_path(:project_id => @project)
  	end
  end

end

QueriesController.send(:include, QueryControllerPatch)