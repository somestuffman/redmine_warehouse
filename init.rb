require_dependency "hook/issue_product"

ActionDispatch::Callbacks.to_prepare do
	require_dependency 'patch/query_controller_patch'
end

Redmine::Plugin.register :redmine_warehouse do
  name 'Redmine Warehouse plugin'
  author 'Semen Formatorov'
  description 'Test task plugin'
  version '0.0.1'
  url ''
  author_url ''
  
  project_module :warehouse do
    permission :view_products_list, { :products => [:index]}, :public => true
    permission :add_products, { :products => [:new, :create]}
    permission :edit_products, { :products => [:edit, :update]}
    permission :destroy_products, { :products => [:destroy]}
  end
  
  menu :project_menu, :redmine_warehouse, { :controller => 'products', :action => 'index' }, :caption => :warehouse, :after => :issues, :param => :project_id
  
end
