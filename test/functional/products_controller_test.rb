require File.expand_path('../../test_helper', __FILE__)

class ProductsControllerTest < ActionController::TestCase
  
  fixtures :projects
  
  Product::TestCase.create_fixtures(Redmine::Plugin.find(:redmine_warehouse).directory + '/test/fixtures/', :products)
  
  def test_index
    @request.session[:user_id] = 1
    
    get :index, :project_id => Project.first
    
    assert_response :success
    assert_template 'index'
    assert_not_nil assigns(:products)
  end
  
  def test_new
    @request.session[:user_id] = 1
    
    get :new, :project_id => Project.first
    
    assert_response :success
    assert_template 'new'
    assert_not_nil assigns(:product)
  end
  
  def test_create
    @request.session[:user_id] = 1
    
    assert_difference 'Product.count' do
      post :create, :project_id => Project.first,
        :product => { :name => "New test product 999",
                      :price => "33",
                      :amount => "45"
                    }
    end
    assert_redirected_to :controller => 'products', :action => 'index', :project_id => Project.first

    product = Product.find_by(:name => "New test product 999")
    assert_not_nil product
    assert_equal 33, product.price
    assert_equal 45, product.amount
    
  end
  
  def test_update
    @request.session[:user_id] = 1

    patch :update, :id => 1, :project_id => Project.first,
      :product => { :name => "New test product 777",
                    :price => "32",
                    :amount => "44"
                  }

    assert_redirected_to :controller => 'products', :action => 'index', :project_id => Project.first

    product = Product.find(1)
    assert_not_nil product
    assert_equal "New test product 777", product.name
    assert_equal 32, product.price
    assert_equal 44, product.amount
  end
  
  def test_show
    @request.session[:user_id] = 1
    
    get :show, :id => 1, :project_id => Project.first
    
    assert_response :success
    assert_template 'show'
  end
  
  def test_edit
    @request.session[:user_id] = 1
    
    get :edit, :id => 1, :project_id => Project.first
    
    assert_response :success
    assert_template 'edit'
    assert_not_nil assigns(:product)

  end
  
  def test_destroy
    @request.session[:user_id] = 1

    assert_difference 'Product.count', -1 do
      delete :destroy, :id => 1, :project_id => Project.first
    end
  end
  
end
