# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

match "/:project_id/warehouse", :controller => "products", :action => "index", :via => :get, :as => 'products'
match "/warehouse/new", :controller => "products", :action => "new", :via => :get, :as => 'new_product'
match "/warehouse/edit/:id", :controller => "products", :action => "edit", :via => :get, :as => 'product_edit'
match "/warehouse/create", :controller => "products", :action => "create", :via => :post, :as => 'create_product'
match "/warehouse/update/:id", :controller => "products", :action => "update", :via => :patch, :as => 'update_product'
match "/warehouse/show/:id", :controller => "products", :action => "show", :via => :get, :as => 'product_show'
match "/warehouse/destroy/:id", :controller => "products", :action => "destroy", :via => :delete, :as => 'destroy_product'