class ProductsController < ApplicationController

  before_action :find_project
  before_action :find_product, :only => [:edit, :update, :show, :destroy]
  
  helper :queries
  helper :sort
  helper :products
  helper Redmine::MenuManager::MenuHelper
  include QueriesHelper
  include SortHelper
  include ProductsHelper

  def index
    retrieve_product_query
    
    if @query.valid?
      respond_to do |format|
        format.html {
          @product_count = @query.products(@project).count
          @product_pages = Paginator.new @product_count, per_page_option, params['page']
          @products = @query.products(@project, {:offset => @product_pages.offset, :limit => @product_pages.per_page})
          render :layout => !request.xhr?
        }
      end
      else
        respond_to do |format|
          format.html { render :layout => !request.xhr? }
        end
      end
      rescue ActiveRecord::RecordNotFound
      render_404
  end


  def new
    @product = Product.new
    @product.issue_id = params[:issue_id] if params[:issue_id]
  end


  def create
    unless User.current.allowed_to?(:add_products, @project, :global => true)
      raise ::Unauthorized
    end
    
    @product = Product.new
    @product.safe_attributes = (params[:product] || {}).deep_dup
    if @product.save
      respond_to do |format|
        format.html {
          flash[:notice] = l(:notice_product_successful_create, :id => view_context.link_to("##{@product.id}", product_show_path(@product, :project_id => @project), :title => @product.name))
          redirect_to :action => 'index', :project_id => @project
        }
      end
      return
    else
      respond_to do |format|
        format.html {
          render :action => 'new'
          }
      end
    end
  end


  def edit
  end


  def update
    @product.safe_attributes = (params[:product] || {}).deep_dup
    if @product.save
      respond_to do |format|
        format.html {
          flash[:notice] = l(:notice_product_successful_updated, :id => view_context.link_to("##{@product.id}", product_show_path(@product, :project_id => @project), :title => @product.name))
          redirect_to :action => 'index', :project_id => @project
        }
      end
      return
    else
      respond_to do |format|
        format.html {
          render :action => 'edit'
          }
      end
    end
  end
  
  def show
    respond_to do |format|
      format.html {
        render :template => 'products/show'
      }
      end
  end


  def destroy
    @product.destroy
    redirect_to :action => 'index', :project_id => @project
  end

  private

  def find_project
    @project = Project.find_by(:identifier => params["project_id"])
  end
  
  def find_product
    @product = Product.find(params[:id])
  end

end
