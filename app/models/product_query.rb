class ProductQuery < Query
  
  self.queried_class = Product
  
  self.available_columns = [
      QueryColumn.new(:id, :sortable => "#{Product.table_name}.id", :default_order => 'desc', :caption => '#', :frozen => true),
      QueryColumn.new(:name, :sortable => "#{Product.table_name}.name"),
      QueryColumn.new(:price, :sortable => "#{Product.table_name}.price"),
      QueryColumn.new(:amount, :sortable => ["#{Product.table_name}.amount"]),
      QueryColumn.new(:production_date, :sortable => "#{Product.table_name}.production_date"),
      QueryColumn.new(:issue_id, :sortable => false, :caption => :label_issue),
      QueryColumn.new(:created_at, :sortable => "#{Product.table_name}.created_at"),
      QueryColumn.new(:updated_at, :sortable => "#{Product.table_name}.updated_at")
    ]
    
    def initialize(attributes=nil, *args)
        super attributes
        self.filters ||= {}
    end
    
    def available_columns
        return @available_columns if @available_columns
        @available_columns = self.class.available_columns.dup
    end
    
    def default_columns_names
      @default_columns_names ||= [:name, :price, :amount]
    end
    
    def initialize_available_filters
      add_available_filter "name", :type => :text
      add_available_filter "price", :type => :integer
      add_available_filter "amount", :type => :integer
      add_available_filter "production_date", :type => :date_past
      add_available_filter "created_at", :type => :date_past
      add_available_filter "updated_at", :type => :date_past
    end
    
    def products(project, options={})
      visible_issues_ids = project.issues.visible.ids + [nil]
      order_option = [group_by_sort_order, (options[:order] || sort_clause)].flatten.reject(&:blank?)
      products = Product.where(:issue_id => visible_issues_ids).where(statement).order(order_option).limit(options[:limit]).offset(options[:offset])
    end
    
    def statement
        # filters clauses
        filters_clauses = []
        filters.each_key do |field|
          v = values_for(field).clone
          next unless v and !v.empty?
          operator = operator_for(field)
          
          if respond_to?(method = "sql_for_#{field.gsub('.','_')}_field")
            # specific statement
            filters_clauses << send(method, field, operator, v)
          else
            # regular field
            filters_clauses << '(' + sql_for_field(field, operator, v, queried_table_name, field) + ')'
          end
        end if filters and valid?

        filters_clauses.reject!(&:blank?)

        filters_clauses.any? ? filters_clauses.join(' AND ') : nil
      end
  
end