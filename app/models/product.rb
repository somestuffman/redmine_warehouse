class Product < ActiveRecord::Base
  include Redmine::SafeAttributes
  
  has_many :issues, :foreign_key => "issue_id"
  
  validates_presence_of :name
  validates_uniqueness_of :name
  
  safe_attributes 'name',
                  'price',
                  'amount',
                  'production_date',
                  'issue_id'
                  
  def safe_attributes=(attrs, user=User.current)
    attrs = delete_unsafe_attributes(attrs, user)
    return if attrs.empty?
    assign_attributes attrs, :without_protection => true
  end
  
end
