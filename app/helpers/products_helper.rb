module ProductsHelper
  
  def product_column_content(column, item)
      value = column.value_object(item)
      value = Issue.find(value).to_s if column.name == :issue_id and value
      if value.is_a?(Array)
        values = value.collect {|v| column_value(column, item, v)}.compact
        safe_join(values, ', ')
      else
        products_column_value(column, item, value)
      end
  end
    
  def products_column_value(column, item, value)
      case column.name
        when :id
          link_to value, product_show_path(item, :project_id => @project)
        when :name
          link_to value, product_show_path(item, :project_id => @project)
      else
          format_object(value)
      end
  end
  
  def retrieve_product_query(klass=ProductQuery, use_session=true)
      session_key = klass.name.underscore.to_sym

      if params[:query_id].present?
        cond = "project_id IS NULL"
        cond << " OR project_id = #{@project.id}" if @project
        @query = klass.where(cond).find(params[:query_id])
        raise ::Unauthorized unless @query.visible?
        @query.project = @project
        session[session_key] = {:id => @query.id, :project_id => @query.project_id} if use_session
      elsif params[:set_filter] || session[session_key].nil?
        @query = klass.new(:name => "_", :project => @project)
        @query.build_from_params(params)
        session[session_key] = {:project_id => @query.project_id, :filters => @query.filters, :group_by => @query.group_by, :column_names => @query.column_names, :totalable_names => @query.totalable_names, :sort => @query.sort_criteria.to_a} if use_session
      else
        # retrieve from session
        @query = nil
        @query = klass.new(:name => "_", :filters => session[session_key][:filters], :group_by => session[session_key][:group_by], :column_names => session[session_key][:column_names], :totalable_names => session[session_key][:totalable_names], :sort_criteria => session[session_key][:sort])
        @query.project = @project
      end
      if params[:sort].present?
        @query.sort_criteria = params[:sort]
        if use_session
          session[session_key] ||= {}
          session[session_key][:sort] = @query.sort_criteria.to_a
        end
      end
      @query
    end

end
