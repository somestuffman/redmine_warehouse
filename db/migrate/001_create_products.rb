class CreateProducts < ActiveRecord::Migration
  
  def change
    create_table :products do |t|
      t.string :name
      t.integer :price
      t.integer :amount
      t.date :production_date
      t.integer :issue_id

      t.timestamps
      
    end
    
    add_foreign_key :products, :issues

  end
end
